(defproject vcomp "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.eclipse.jgit/org.eclipse.jgit "4.4.1.201607150455-r"]
                 [org.eclipse.aether/aether-api "1.1.0"]
                 [org.eclipse.aether/aether-util "1.1.0"]
                 [org.eclipse.aether/aether-impl "1.1.0"]
                 [org.eclipse.aether/aether-connector-basic "1.1.0"]
                 [org.eclipse.aether/aether-transport-http "1.1.0"]
                 [org.apache.maven/maven-aether-provider "3.3.9"]
                 [org.clojure/math.combinatorics "0.1.3"]
                 [org.clojure/tools.cli "0.3.5"]]
  :jvm-opts ["-Djavax.net.ssl.trustStore=C:\\Users\\tkw768\\cacerts" "-Djavax.net.ssl.trustStorePassword=changeit"])
