# vcomp

Compare dependencies of maven artifacts.

## Usage

vcomp [options] OLD_GAEV NEW_GAEV

Options:
-h --help Show help
-c --config CONFIG Configuration file path

GAEV is maven's Group:Artifact:Extension:Version
Example:
com.motorolasolutions.pcm:cm:war:5.4.12

## License

Copyright � 2016 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
