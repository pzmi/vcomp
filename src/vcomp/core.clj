(ns vcomp.core
  (:gen-class)
  (:require [clojure.math.combinatorics :refer [cartesian-product]]
            [clojure.tools.cli :as cli]
            [clojure.java.io :as io]
            [clojure.string :as string]
            [clojure.edn :as edn])
  (:import (org.apache.maven.repository.internal MavenRepositorySystemUtils)
           (org.eclipse.aether.spi.connector RepositoryConnectorFactory)
           (org.eclipse.aether.connector.basic BasicRepositoryConnectorFactory)
           (org.eclipse.aether.spi.connector.transport TransporterFactory)
           (org.eclipse.aether.transport.http HttpTransporterFactory)
           (org.eclipse.aether RepositorySystem)
           (org.eclipse.aether.repository LocalRepository RemoteRepository$Builder)
           (org.eclipse.aether.graph Dependency)
           (org.eclipse.aether.artifact DefaultArtifact)
           (org.eclipse.aether.collection CollectRequest)
           (org.eclipse.aether.resolution DependencyRequest DependencyResult)
           (org.eclipse.aether.util.repository AuthenticationBuilder)))

(defn- system []
  (let [locator (doto (MavenRepositorySystemUtils/newServiceLocator)
                  (.addService RepositoryConnectorFactory BasicRepositoryConnectorFactory)
                  (.addService TransporterFactory HttpTransporterFactory))]
    (.getService locator RepositorySystem)))

(defonce loc-rep (str (System/getProperty "user.home") "/.m2/repository"))

(defn- session [^RepositorySystem system]
  (let [s (MavenRepositorySystemUtils/newSession)
        local (new LocalRepository loc-rep)]
    (doto s
      (.setLocalRepositoryManager (.newLocalRepositoryManager system s local)))))

(defn- repo-builder [name profile url]
  (new RemoteRepository$Builder name profile url))

(defn- auth-builder [username password]
  (.. (new AuthenticationBuilder)
      (addUsername username)
      (addPassword password)
      (build)))

(defn repo
  ([name profile url]
   (.build (repo-builder name profile url)))
  ([name profile url username password]
   (.build (.setAuthentication (repo-builder name profile url) (auth-builder username password)))))

(defonce central
         (repo "central" "default" "http://repo1.maven.org/maven2/"))

(defn- dependency
  ([gaev]
   (new Dependency (new DefaultArtifact gaev) "compile")))

(defn- col-req [dep repo]
  (doto (new CollectRequest)
    (.setRoot dep)
    (.addRepository repo)))

(defn- node [system session col-req]
  (.. system
      (collectDependencies session col-req)
      getRoot))

(defn- dep-req [node]
  (doto (new DependencyRequest)
    (.setRoot node)))

(defn- res-deps [system session request]
  (.resolveDependencies system session request))

(defn- art->vart [art]
  {:group (.getGroupId art) :artifact (.getArtifactId art) :version (.getVersion art)})

(defn- get-deps
  ([gaev repo]
   (let [sys (system)
         sess (session sys)
         col (col-req (dependency gaev) repo)
         dn (node sys sess col)
         req (dep-req dn)]
     (res-deps sys sess req)))
  ([gaev]
   (get-deps gaev (central))))

(defn- deps->varts [^DependencyResult deps]
  (map art->vart
       (map #(.getArtifact %)
            (.getArtifactResults deps))))

(defn get-varts
  ([gaev repo]
   (deps->varts (get-deps gaev repo)))
  ([gaev]
   (get-varts gaev (central))))

(defn added [o n]
  (let [oa (set (map :artifact o))]
    (filter #(not (contains? oa (:artifact %))) n)))

(defn deleted [o n]
  (added n o))

(defn- upgraded? [ont]
  (let [ov (:version (first ont))
        nv (:version (second ont))]
    (= -1 (compare ov nv))))

(defn- with-upgrade [ont]
  (let [o (first ont)
        n (second ont)]
    {:group (:group n) :artifact (:artifact n) :upgrade (str (:version o) "->" (:version n))}))

(defn upgraded [o n]
  (let [pairs (cartesian-product o n)]
    (map with-upgrade (filter upgraded? (filter #(= (:artifact (first %)) (:artifact (second %))) pairs)))))

(defonce cli-options
         ([["-h" "--help"]
           ["-c" "--config CONFIG" "Configuration file path"
            :default "conf.edn"
            :validate #(.exists (io/as-file %)) "Given config file does not exist"]
           ]))


(defn- usage [options-summary]
  (->> ["Compare dependencies of maven artifacts."
        ""
        "Usage: vcomp [options] OLD_GAEV NEW_GAEV"
        ""
        "Options:"
        options-summary
        ""
        "GAEV is maven's Group:Artifact:Extension:Version"
        "Example:"
        "com.motorolasolutions.pcm:cm:war:5.4.12"]
       (string/join \newline)))

(defn- error-msg [errors]
  (str "The following errors occurred while parsing your command:\n\n"
       (string/join \newline errors)))

(defn- exit [status msg]
  (println msg)
  (System/exit status))

(defn- vcomp [arguments config]
  (let [user (:username config)
        pass (:password config)
        url (:url config)
        r (repo "remote" "default" url user pass)
        old (get-varts (first arguments) r)
        new (get-varts (second arguments) r)]
    (println
      (->> ["Added"
          (str (added old new))
          "Removed"
          (str (deleted old new))
          "Upgraded"
          (str (upgraded old new))]
         (string/join \newline)))))

(defn- read-config [file]
  (edn/read-string (slurp file)))

(defn -main [& args]
  (let [{:keys [options arguments errors summary]} (cli/parse-opts args cli-options)]
    (cond
      (:help options) (exit 0 (usage summary))
      (not= (count arguments) 2) (exit 1 (usage summary))
      errors (exit 1 (error-msg errors)))
    (if (= 2 (count arguments))
      (vcomp arguments (read-config (:config options)))
      (exit 1 (usage summary)))))